package com.xiaohuzi.test.constant;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BasicRetCodeTest {

    @Test
    public void ret() {

        String str = null;
        str = "";

        if (null != str && !str.isEmpty()) {
            System.out.println(str);
        } else {
            System.out.println("nll");
        }

        Assert.assertEquals(BasicRetCode.SUCCESS.ret(), 0);
        Assert.assertEquals(BasicRetCode.PERMISSION_DENY.ret(), -5);

    }

    @Test
    public void defaultMsg() {
        Assert.assertEquals(BasicRetCode.SUCCESS.defaultMsg(), "ok");
    }
}