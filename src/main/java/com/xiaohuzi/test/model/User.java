package com.xiaohuzi.test.model;

import com.xiaohuzi.test.component.auth.Authenticatable;
import com.xiaohuzi.test.constant.Gender;
import lombok.Data;

@Data
/**
 * lombok 编译是自动生成get/set
 */
public class User implements Authenticatable {

    private static final long serialVersionUid = 1L;
    private Long id;
    private String userName;
    private String passWord;
    private Gender gender;
    private String nickName;
    private Integer num;
//    private String num;

    @Override
    public long getAuthIdentifier() {
        return 0;
    }

    @Override
    public String getAuthPassword() {
        return null;
    }

//    public User() {
//        super();
//        userName = "java";
//    }

//    public User(String userName, String passWord, Gender gender) {
//        super();
//        this.userName = userName;
//        this.passWord = passWord;
//        this.gender = gender;
//    }

//    public Long getId() {
//        return this.id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getUserName() {
//        return this.userName;
//    }
//
//    public void setUserName(String userName) {
//        this.userName = userName;
//    }
//
//    public String getPassWord() {
//        return this.passWord;
//    }
//
//    public void setPassWord(String passWord) {
//        this.passWord = passWord;
//    }
//
//    public Gender getgender() {
//        return this.gender;
//    }
//
//    public void setGender(Gender gender) {
//        this.gender = gender;
//    }
//
//    public String getNickName() {
//        return this.nickName;
//    }
//
//    public void setNickName(String nickName) {
//        this.nickName = nickName;
//    }
//
//    @Override
//    public String toString() {
//        return "userName1111: " + this.userName + ", passWord: " + this.passWord + ", gender: " + this.gender;
//    }


}
