package com.xiaohuzi.test.interceptor;

import com.xiaohuzi.test.component.entity.HeaderEntity;
import com.xiaohuzi.test.util.HeaderUtil;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 填充Header信息
 *
 * @author xiaohuzi
 */
public class HeaderInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HeaderEntity header = HeaderUtil.getHttpHeaderAttrFromRequest(request);

        if (null != header) {
            return true;
        }

        header = HeaderUtil.buildFromHttp(request, null);
        HeaderUtil.setHttpHeaderAttr(request, header);

        return true;
    }
}
