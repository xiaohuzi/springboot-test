package com.xiaohuzi.test.interceptor;

import com.google.common.base.Strings;
import com.xiaohuzi.test.constant.RouteInterceptor;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

/**
 * 认证拦截器
 *
 * @author xiaohuzi
 */
public class AuthInterceptor implements HandlerInterceptor {

    private RouteInterceptor[] interceptors;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        Object obj = request.getAttribute("interceptors");

        if (obj == null) {
            return true;
        }

        interceptors = (RouteInterceptor[]) obj;

        if (-1 == Arrays.binarySearch(interceptors, RouteInterceptor.AUTH)) {
            return true;
        }

        HandlerMethod handlerMethod;
        handlerMethod = (HandlerMethod) handler;


        //do more


        //开始权限检查
//        Cookie[] cookies;
//        cookies = request.getCookies();
//        String uid = null;
//
//        for (Cookie cookie : cookies) {
//            if ("uid".contains(cookie.getName()) && !Strings.isNullOrEmpty(cookie.getValue())) {
//                uid = cookie.getValue();
//            }
//        }

        return true;
    }
}
