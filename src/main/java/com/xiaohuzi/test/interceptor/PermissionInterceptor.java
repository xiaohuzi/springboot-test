package com.xiaohuzi.test.interceptor;


import com.xiaohuzi.test.Route;
import com.xiaohuzi.test.constant.RouteInterceptor;
import com.xiaohuzi.test.util.AntPathUtil;

import org.springframework.util.AntPathMatcher;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Map;


/**
 * 权限拦截器
 *
 * @author xiaohuzi
 */
public class PermissionInterceptor implements HandlerInterceptor {

    private RouteInterceptor[] interceptors;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        Object obj = request.getAttribute("interceptors");

        if (obj == null) {
            return true;
        }

        interceptors = (RouteInterceptor[]) obj;

        if (-1 == Arrays.binarySearch(interceptors, RouteInterceptor.PERMISSION)) {
            return true;
        }

        if (!(handler instanceof HandlerMethod)) {
            return true;
        }

        HandlerMethod handlerMethod;
        handlerMethod = (HandlerMethod) handler;



        //do more

        return true;
    }

}
