package com.xiaohuzi.test.interceptor;

import com.xiaohuzi.test.component.route.RouteHelp;
import com.xiaohuzi.test.constant.RouteInterceptor;
import com.xiaohuzi.test.util.AntPathUtil;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;

/**
 * 路由拦截器
 * 强制所有请求的uri都在com/xiaohuzi/test/Route.java文件里进行配置
 * 需要首先注册当前拦截器
 *
 * @author xiaohuzi
 */
public class DispatchInterceptor implements HandlerInterceptor {

    private RouteInterceptor[] interceptors;


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        String uri;
        uri = request.getRequestURI();
        Map<String, RouteInterceptor[]> interceptorMap;
        interceptorMap = RouteHelp.getRouteInterceptor();
        interceptors = interceptorMap.get(uri);

        Map<String, RouteInterceptor[]> interceptorCache;
        interceptorCache = AntPathUtil.antRouteInterceptorCache();

        if (interceptors == null) {
            interceptors = interceptorCache.get(uri);
        }

        if (interceptors == null) {
            AntPathMatcher antPathUtil = AntPathUtil.instance();
            interceptorMap = RouteHelp.getAntRouteInterceptor();

            interceptorMap.forEach((antUri, antInterceptor) -> {
                if (antPathUtil.match(antUri, uri)) {
                    interceptors = antInterceptor;
                    AntPathUtil.antRouteInterceptorCache().put(uri, antInterceptor);
                    return;
                }
            });
        }

        if (!Objects.isNull(interceptors)) {
            Arrays.sort(interceptors);
        }
        //向下游拦截器传递当前uri，需要参与拦截的拦截器
        request.setAttribute("interceptors", interceptors);

        return true;
    }
}
