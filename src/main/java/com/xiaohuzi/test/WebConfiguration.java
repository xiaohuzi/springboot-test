package com.xiaohuzi.test;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.hubspot.jackson.datatype.protobuf.ProtobufModule;
import com.xiaohuzi.test.interceptor.AuthInterceptor;
import com.xiaohuzi.test.interceptor.HeaderInterceptor;
import com.xiaohuzi.test.interceptor.PermissionInterceptor;
import com.xiaohuzi.test.interceptor.DispatchInterceptor;

import java.io.IOException;
import java.util.Collections;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.catalina.filters.RemoteIpFilter;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.protobuf.ProtobufHttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/**
 * Spring Boot 支持基于 Java 的配置。
 * 虽然可以在 SpringApplication 中使用 XML 配置源，但我们通常建议主配置源为 @Configuration 类。
 * 通常，一个很好的选择是将定义了 main 方法的类作为 @Configuration
 *
 * @Configuation 等价于 <Beans></Beans>
 * @Bean 等价于 <Bean></Bean>
 * @ComponentScan 等价于<context:component-scan base-package=”com.xiaohuzi.test”/>
 */
@Configuration
public class WebConfiguration implements WebMvcConfigurer {


    /**
     * protobuf 序列化
     */
    @Bean
    ProtobufHttpMessageConverter protobufHttpMessageConverter() {
        return new ProtobufHttpMessageConverter();
    }

    /**
     * protobuf 反序列化
     */
    @Bean
    RestTemplate restTemplate(ProtobufHttpMessageConverter protobufHttpMessageConverter) {
        return new RestTemplate(Collections.singletonList(protobufHttpMessageConverter));
    }


    /**
     * json 请求需求序列化
     *
     * @return
     */
    @Bean
    @SuppressWarnings("unchecked")
    public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer() {
        return jacksonObjectMapperBuilder -> {
            jacksonObjectMapperBuilder.featuresToDisable(
                    JsonGenerator.Feature.IGNORE_UNKNOWN,
                    MapperFeature.DEFAULT_VIEW_INCLUSION,
                    DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
                    SerializationFeature.WRITE_DATES_AS_TIMESTAMPS
            );
            jacksonObjectMapperBuilder.propertyNamingStrategy(PropertyNamingStrategy.LOWER_CAMEL_CASE);//如果字段都是驼峰命名规则，需要这一句
            jacksonObjectMapperBuilder.modulesToInstall(ProtobufModule.class);
        };
    }


    /**
     * 注册拦截器
     *
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new DispatchInterceptor());//DispatchInterceptor 一定要放在第一个位置注册
        registry.addInterceptor(new HeaderInterceptor());
        registry.addInterceptor(new AuthInterceptor());
        registry.addInterceptor(new PermissionInterceptor());
    }

    /**
     * 注册过滤器
     *
     * @return
     */
    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new MyFilter());
        registration.addUrlPatterns("/*");
        registration.addInitParameter("paramName", "paramValue");
        registration.setName("MyFilter");
        registration.setOrder(1);

        return registration;
    }

    public class MyFilter implements Filter {

        @Override
        public void destroy() {

        }

        @Override
        public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
            HttpServletRequest request = (HttpServletRequest) servletRequest;
            System.out.println("MyFilter Print: " + request.getRequestURL());

            filterChain.doFilter(request, servletResponse);
        }

        @Override
        public void init(FilterConfig filterConfig) throws ServletException {

        }
    }


    @Bean
    public RemoteIpFilter remoteIpFilter() {
        System.out.println("filter");
        return new RemoteIpFilter();
    }

}
