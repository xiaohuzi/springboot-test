package com.xiaohuzi.test.message;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(builderClassName = "Builder", toBuilder = true)
public class Form {
    private String username;
    private Integer type;
    private Long applyId;
}
