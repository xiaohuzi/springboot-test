package com.xiaohuzi.test.repository;

/**
 * mybatis 以注解方式操作数据库
 */

import java.util.List;

import com.xiaohuzi.test.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.Select;

import com.xiaohuzi.test.constant.Gender;

public interface UserMapper {

    /* @Result 修饰返回的结果集，关联实体类属性和数据库字段一一对应，如果实体类属性和数据库属性名保持一致，就不需要这个属性来修饰。 */

    @Select("select * from users")
    @Results({
            @Result(property = "gender", column = "user_sex", javaType = Gender.class),
            @Result(property = "nickName", column = "nick_name")
    })
    List<User> getAll();

    @Select("select * from users where id = #{id}")
    @Results({
            @Result(property = "gender", column = "user_sex", javaType = Gender.class),
            @Result(property = "nickName", column = "nick_name")
    })
    User findOne(Long id);

    @Insert("insert into users(userName, passWord, user_sex) values(#{userName}, #{passWord}, #{gender})")
    void add(User user);

    @Update("update users set userName = #{userName}, passWord = #{passWord}, user_sex = #{gender} where id = #{id}")
    int update(User user);

    @Delete("delete from users where id = #{id}")
    void delete(Long id);

}


