package com.xiaohuzi.test.repository;

import com.xiaohuzi.test.component.auth.Authenticatable;
import com.xiaohuzi.test.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository {

    List<User> list(int offset, int rowCount);

    User findOne(Long id);

    void add(User user);

    void update(User user);

    void delete(Long id);

    List<User> getAll();

    Authenticatable retrieveById(long identifier);
}
