package com.xiaohuzi.test;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Objects;

/**
 * @SpringBootApplication 注解相当于使用 @Configuration、@EnableAutoConfiguration 和 @ComponentScan 及他们的默认属性
 */
@SpringBootApplication
@MapperScan("com.xiaohuzi.test.repository")
public class Application {

    static {
        if (Objects.isNull(System.getProperty("spring.profiles.active"))
                || System.getProperty("spring.profiles.active").isEmpty()) {
            System.setProperty("spring.profiles.active", "dev");
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
