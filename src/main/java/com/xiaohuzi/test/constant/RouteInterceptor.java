package com.xiaohuzi.test.constant;

/**
 * 路由拦截器枚举
 *
 * @author xiaohuzi
 */
public enum RouteInterceptor {
    /**
     * 请求的uri地址不需要拦截
     */
    DEFAULT("NoInterceptor"),

    /**
     * 认证拦截器
     */
    AUTH("AuthInterceptor"),

    /**
     * 鉴权拦截器
     */
    PERMISSION("PermissionInterceptor");

    private final String className;

    RouteInterceptor(String className) {
        this.className = className;
    }

    public String className() {
        return this.className;
    }
}
