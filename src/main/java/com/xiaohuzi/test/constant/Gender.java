package com.xiaohuzi.test.constant;

import com.xiaohuzi.test.message.user.UserEntity;

import java.util.HashMap;
import java.util.Map;

public enum Gender{
    MAN, WOMAN;

    Map<Gender, UserEntity.Gender> protoMap = new HashMap<>();

    public UserEntity.Gender toProto() {
        if (protoMap.size() == 0) {
           initMap();
        }

        return protoMap.get(this);
    }

    private void initMap() {
        protoMap.put(MAN, UserEntity.Gender.MAN);
        protoMap.put(WOMAN, UserEntity.Gender.WOMAN);
    }

}
