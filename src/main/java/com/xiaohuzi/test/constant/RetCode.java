package com.xiaohuzi.test.constant;

/**
 * 返回状态码接口
 *
 * @author xiaohuzi
 */
public interface RetCode {

    /**
     * 结果码
     *
     * @return
     */
    int ret();

    /**
     * 默认消息
     *
     * @return
     */
    String defaultMsg();
}
