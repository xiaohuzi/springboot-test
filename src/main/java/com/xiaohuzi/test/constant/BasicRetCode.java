package com.xiaohuzi.test.constant;

/**
 * 基础返回状态码
 *
 * @author xiaohuzi
 */
public enum BasicRetCode implements RetCode {

    /* 通用返回状态码 0 ~ 100 */
    SUCCESS(0, "ok"),
    ERROR_UNKNOWN(-1, "系统异常，请稍后重试"),
    PARAM_INVALID(-2, "请求参数不正确"),
    NEED_LOGIN(-3, "请登录后再操作"),
    ACCESS_RESTRICT(-4, "访问受限"),
    PERMISSION_DENY(-5, "没有权限");

    /* 10xx - 系统错误 */
    //@todo


    private final int retCode;
    private final String defaultMsg;

    BasicRetCode(int retCode, String defaultMsg) {
        this.retCode = retCode;
        this.defaultMsg = defaultMsg;
    }

    @Override
    public int ret() {
        return retCode;
    }

    @Override
    public String defaultMsg() {
        return defaultMsg;
    }

}
