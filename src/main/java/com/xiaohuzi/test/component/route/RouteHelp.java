package com.xiaohuzi.test.component.route;

import com.xiaohuzi.test.component.entity.RouteEntity;
import com.xiaohuzi.test.constant.RouteInterceptor;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 路由助手，populate参与路由过程的4个元素
 *
 * @author xiaohuzi
 */
@Component
public class RouteHelp {
    /**
     * url&action的路由表
     */
    private static final Map<String, Map> routeMap = new LinkedHashMap<>();

    /**
     * url请求前缀映射
     */
    private static final Map<String, String> prefixMap = new LinkedHashMap<>();

    /**
     * url生效拦截器映射
     */
    private static final Map<String, RouteInterceptor[]> routeInterceptor = new LinkedHashMap<>();

    /**
     * ant url生效拦截器映射
     */
    private static final Map<String, RouteInterceptor[]> antRouteInterceptor = new LinkedHashMap<>();

    /**
     * 当前解析的路由组
     */
    private String currentNamespace;


    public RouteHelp namespace(String namespace) {
        currentNamespace = namespace;
        return this;
    }

    public RouteHelp prefix(String prefix) {
        if (currentNamespace.isEmpty()) {
            return null;
        }
        prefixMap.put(currentNamespace, prefix);
        return this;
    }

    public RouteHelp interceptor(RouteInterceptor[] inter) {
        if (currentNamespace.isEmpty()) {
            return null;
        }
        routeInterceptor.put(currentNamespace, inter);
        return this;
    }

    /**
     * populate 路由
     *
     * @param routeMapping
     */
    public void route(RouteMapping routeMapping) {
        Map<String, RouteEntity> route = new LinkedHashMap<>();
        routeMap.put(currentNamespace, route);
        routeMapping.mapping(route);
        currentNamespace = "";
    }

    public Map<String, Map> getRouteMap() {
        return routeMap;
    }

    public Map<String, String> getPrefixMap() {
        return prefixMap;
    }

    public static Map<String, RouteInterceptor[]> getRouteInterceptor() {
        return routeInterceptor;
    }

    public static Map<String, RouteInterceptor[]> getAntRouteInterceptor() {
        return antRouteInterceptor;
    }

}
