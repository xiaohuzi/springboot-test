package com.xiaohuzi.test.component.route;

import com.xiaohuzi.test.component.entity.RouteEntity;

import java.util.Map;

/**
 * 路由函数接口, 填充路由配置文件
 *
 * @author xiaohuzi
 */
public interface RouteMapping {
    void mapping(Map<String, RouteEntity> route);
}
