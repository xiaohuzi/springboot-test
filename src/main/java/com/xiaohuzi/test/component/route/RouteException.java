package com.xiaohuzi.test.component.route;

/**
 * 路由Exception
 *
 * @author xiaohuzi
 */
public class RouteException extends Exception {
    public RouteException(String message) {
        super(message);
    }
}
