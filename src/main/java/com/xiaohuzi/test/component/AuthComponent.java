package com.xiaohuzi.test.component;

import com.xiaohuzi.test.component.auth.AuthProvider;
import com.xiaohuzi.test.component.auth.Authenticatable;
import com.xiaohuzi.test.util.crypt.md5;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * 认证组件
 *
 * @author xiaohuzi
 */
@Component
public class AuthComponent {

    /**
     * auth config info
     */
    private AuthProvider provider;

    @Autowired
    public AuthComponent(AuthProvider provider) {
        System.out.println("auth init");
        this.provider = provider;
    }


    /**
     * get request
     *
     * @return
     */
    private HttpServletRequest getRequest() {
        HttpServletRequest request = ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getRequest();
        return request;
    }


    /**
     * get response
     *
     * @return
     */
    private HttpServletResponse getResponse() {
        HttpServletResponse response = ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getResponse();
        return response;
    }

    /**
     * get session
     *
     * @return
     */
    private HttpSession getSession() {
        HttpSession session = getRequest().getSession();
        System.out.println(session);
        return session;
    }

    /**
     * login
     *
     * @param user
     * @throws Exception
     */
    public void login(Authenticatable user) throws Exception {
        updateSession(user.getAuthIdentifier());
        setUser(user);
    }

    /**
     * logout
     *
     * @throws Exception
     */
    public void logout() throws Exception {
        clearUserDataFromStorage();
        getRequest().setAttribute("user", null);
    }


    /**
     * Determine if the current user is authenticated.
     *
     * @return bool
     */
    public boolean check() throws Exception {
        return user() != null;
    }


    /**
     * Determine if the current user is a guest.
     *
     * @return bool
     */
    public boolean guest() throws Exception {
        return !check();
    }


    /**
     * Get a unique identifier for the auth session value.
     *
     * @return string
     */
    public String getName() throws Exception {
        String md5Str;
        md5Str = md5.crypt(this.getClass().getName());

        return "login_" + md5Str;
    }


    /**
     * Migrates the current session to a new session id
     */
    public void updateSession(long id) throws Exception {
        getSession().invalidate();
        getSession().setAttribute(getName(), id);
    }


    /**
     * cache user
     *
     * @param user
     */
    public void setUser(Authenticatable user) {
        getRequest().setAttribute("user", user);
    }


    /**
     * Remove the user data from the session and cookies.
     *
     * @return void
     */
    protected void clearUserDataFromStorage() throws Exception {
        getSession().removeAttribute(getName());
        Cookie[] cookies = getRequest().getCookies();

        for (Cookie cookie : cookies) {
            cookie.setValue("");
            cookie.setMaxAge(0);
            cookie.setPath("/");
//            cookie.setDomain(domain);
            getResponse().addCookie(cookie);
        }
    }


    /**
     * Get the currently authenticated user.
     *
     * @see com.xiaohuzi.test.component.auth.Authenticatable
     */
    public Authenticatable user() throws Exception {
        Object obj = getRequest().getAttribute("user");

        if (obj != null) {
            return (Authenticatable)obj;
        }

        if (getSession().getAttribute(getName()) == null) {
            return null;
        }

        long id = (long) getSession().getAttribute(getName());

//        getSession();
//        long id;
//        id = 29;

        Authenticatable user;
        user = provider.retrieveById(id);

        setUser(user);

        return user;
    }

}
