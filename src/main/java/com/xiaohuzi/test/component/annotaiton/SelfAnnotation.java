package com.xiaohuzi.test.component.annotaiton;

import java.lang.annotation.*;

/**
 * 注解定义
 * 参见 https://blog.csdn.net/javazejian/article/details/71860633
 * @author xiaohuzi
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SelfAnnotation {

    /**
     * 参数1
     * @return
     */
    String param1();

    /**
     * 参数2， 默认值 ""
     * @return
     */
    String param2() default "";
}
