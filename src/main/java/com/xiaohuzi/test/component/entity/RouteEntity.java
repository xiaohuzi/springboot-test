package com.xiaohuzi.test.component.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.web.bind.annotation.RequestMethod;


@Data
@Builder
public class RouteEntity {

    @Builder.Default
    private String name = "";
    @Builder.Default
    private String[] value = new String[]{};
    @Builder.Default
    private String[] path = new String[]{};
    @Builder.Default
    private RequestMethod[] method = new RequestMethod[]{RequestMethod.POST};
    @Builder.Default
    private String[] params = new String[]{};
    @Builder.Default
    private String[] headers = new String[]{};
    @Builder.Default
    private String[] consumes = new String[]{};
    @Builder.Default
    private String[] produces = new String[]{};
//    private String[] produces = new String[]{"application/x-protobuf"};
}
