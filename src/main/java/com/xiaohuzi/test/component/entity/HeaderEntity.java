package com.xiaohuzi.test.component.entity;

import lombok.Data;
import lombok.Builder;

@Data
@Builder(builderClassName = "Builder", toBuilder = true)
public class HeaderEntity {

    /**
     * 请求的url
     */
    private final String requestUrl;

    /**
     * 请求的方法
     */
    private final String requestMethod;

    /**
     * 来源IP
     */
    private final String remoteIp;

    /**
     * user agent
     */
    private final String userAgent;

    /**
     * referer
     */
    private final String referer;

    /**
     * 用户id
     */
    private final Long uid;

}
