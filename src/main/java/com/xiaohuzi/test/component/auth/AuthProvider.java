package com.xiaohuzi.test.component.auth;

/**
 * user 数据操作需要实现该接口
 */
public interface AuthProvider {

    /**
     * Retrieve a user by their unique identifier.
     */
    Authenticatable retrieveById(long identifier);
}
