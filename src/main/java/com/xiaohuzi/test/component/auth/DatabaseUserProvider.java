package com.xiaohuzi.test.component.auth;

import com.xiaohuzi.test.repository.UserRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class DatabaseUserProvider implements AuthProvider{

    @Resource
    private UserRepository userRepository;

    public Authenticatable retrieveById(long identifier) {
        return userRepository.retrieveById(identifier);
    }

}
