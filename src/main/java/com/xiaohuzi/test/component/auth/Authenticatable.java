package com.xiaohuzi.test.component.auth;

/**
 * 定义可认证的User数据实例行为
 *
 * @author xiaohuzi
 */
public interface Authenticatable {

    /**
     * Get the unique identifier for the user.
     */
    long getAuthIdentifier();

    /**
     * Get the password for the user.
     */
    String getAuthPassword();

}
