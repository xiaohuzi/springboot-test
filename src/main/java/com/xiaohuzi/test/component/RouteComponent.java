package com.xiaohuzi.test.component;

import com.xiaohuzi.test.Route;
import com.xiaohuzi.test.component.entity.RouteEntity;
import com.xiaohuzi.test.component.route.RouteHelp;
import com.xiaohuzi.test.constant.RouteInterceptor;
import com.xiaohuzi.test.util.AntPathUtil;
import org.springframework.core.annotation.AnnotationConfigurationException;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.condition.RequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Route解析与@RequestMapping装配操作
 *
 * @author xiaohuzi
 */
@Component
public class RouteComponent extends RequestMappingHandlerMapping {

    private final Map<String, Map> routeMap;
    private final Map<String, String> prefixMap;
    private final Map<String, RouteInterceptor[]> routeInterceptor;
    private final Map<String, RouteInterceptor[]> antRouteInterceptor;

    private RequestMappingInfo requestMappingInfo;
    private RouteHelp routeHelp;

    public RouteComponent(Route route) {
        route.mapping();
        routeHelp = route.getRouteHelp();
        routeMap = routeHelp.getRouteMap();
        prefixMap = routeHelp.getPrefixMap();
        routeInterceptor = routeHelp.getRouteInterceptor();
        antRouteInterceptor = routeHelp.getAntRouteInterceptor();
    }


    @Nullable
    @Override
    protected RequestMappingInfo getMappingForMethod(Method method, Class<?> handlerType) {
        RequestMappingInfo info;

        info = super.getMappingForMethod(method, handlerType);

        if (info == null) {
            boolean ret;
            ret = route(method, handlerType);
            if (ret) {
                info = requestMappingInfo;
            }
        }

        return info;
    }

    /**
     * 路由
     *
     * @param method
     * @param handlerType
     * @return
     */
    private boolean route(Method method, Class<?> handlerType) {
        String clazz;
        clazz = handlerType.getName();
        int lastFlag;
        lastFlag = clazz.lastIndexOf(".");
        String namespace;
        namespace = clazz.substring(0, clazz.lastIndexOf("."));
        String className;
        className = clazz.substring(lastFlag + 1);
        String methodName;
        methodName = method.getName();

        Map<String, RouteEntity> routeMapping;
        routeMapping = routeMap.get(namespace);

        if (routeMapping == null) {
            return false;
        }

        String prefix;
        prefix = prefixMap.get(namespace);
        RouteEntity routeEntity;
        routeEntity = routeMapping.get(className + "@" + methodName);

        if (routeEntity == null) {
            if (method.getModifiers() == Modifier.PUBLIC) {
                throw new AnnotationConfigurationException("RequestMapping annotation configure error, RouteEntity don't set");
            } else {
                return false;
            }
        }

        RequestMapping requestMapping;
        requestMapping = makeRequestMapping(routeEntity, prefix);
        AnnotatedElement element;
        element = RequestMapping.class;

        RequestCondition<?> condition = element instanceof Class ? this.getCustomTypeCondition((Class) element) : this.getCustomMethodCondition((Method) element);
        requestMappingInfo = requestMapping != null ? this.createRequestMappingInfo(requestMapping, condition) : null;

        try {
            // The map has to be built for the first time
            Class<?> superclass;
            superclass = method.getClass().getSuperclass();
            Field declaredField;
            declaredField = superclass.getDeclaredField("declaredAnnotations");
            declaredField.setAccessible(true);

            @SuppressWarnings("unchecked")
            Map<Class<? extends Annotation>, Annotation> annotations;
            annotations = (Map<Class<? extends Annotation>, Annotation>) declaredField.get(method);

            if (annotations.isEmpty()) {
                declaredField.set(method, new LinkedHashMap<Class<? extends Annotation>, Annotation>());
                annotations = (Map<Class<? extends Annotation>, Annotation>) declaredField.get(method);
            }

            annotations.put(RequestMapping.class, requestMapping);

        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


        populate(requestMapping.value(), namespace);

        return true;
    }


    /**
     * 从route配置项中解析RequestMapping
     *
     * @param routeEntity
     * @param prefix
     * @return
     */
    private RequestMapping makeRequestMapping(RouteEntity routeEntity, String prefix) {
        String[] value = routeEntity.getValue();
        String[] path = routeEntity.getPath();

        if (value.length == 0 && path.length == 0) {
            throw new AnnotationConfigurationException("RequestMapping annotation configure error, value & path is empty");
        }

        value = value.length == 0 ? path : value;
        path = path.length == 0 ? value : path;

        if (!Arrays.equals(value, path)) {
            throw new AnnotationConfigurationException("RequestMapping annotation configure error, value & path is not equality");
        }

        // 拼接 uri，过滤prefix、RequestMap.value 前后的/
        for (int i = 0; i < value.length; i++) {
            value[i] = prefix + "/" + value[i];
            value[i] = value[i].replaceAll("/{2,3}", "/");
        }

        routeEntity.setValue(value);

        return requestMapping(routeEntity);
    }


    /**
     * 填充路由具体uri拦截器
     *
     * @param uri
     * @param namespace
     */
    private void populate(String[] uri, String namespace) {

        for (int i = 0; i < uri.length; i++) {
            if (AntPathUtil.instance().isPattern(uri[i])) {
                antRouteInterceptor.put(uri[i], routeInterceptor.get(namespace) != null ? routeInterceptor.get(namespace) : new RouteInterceptor[]{RouteInterceptor.DEFAULT});
            } else {
                routeInterceptor.put(uri[i], routeInterceptor.get(namespace) != null ? routeInterceptor.get(namespace) : new RouteInterceptor[]{RouteInterceptor.DEFAULT});
            }

        }
    }


    private RequestMapping requestMapping(RouteEntity routeEntity) {
        return new RequestMapping() {

            @Override
            public Class<? extends Annotation> annotationType() {
                return RequestMapping.class;
            }

            @Override
            public String name() {
                return routeEntity.getName();
            }

            @Override
            public String[] value() {
                return routeEntity.getValue();
            }

            @Override
            public String[] path() {
                return routeEntity.getValue();
            }

            @Override
            public RequestMethod[] method() {
                return routeEntity.getMethod();
            }

            @Override
            public String[] params() {
                return routeEntity.getParams();
            }

            @Override
            public String[] headers() {
                return routeEntity.getHeaders();
            }

            @Override
            public String[] consumes() {
                return routeEntity.getConsumes();
            }

            @Override
            public String[] produces() {
                return routeEntity.getProduces();
            }
        };


    }


}
