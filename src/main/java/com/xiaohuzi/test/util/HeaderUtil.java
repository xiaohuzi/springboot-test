package com.xiaohuzi.test.util;

import com.google.common.base.Strings;
import com.google.common.net.HttpHeaders;
import com.google.common.net.InetAddresses;
import com.xiaohuzi.test.component.entity.HeaderEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;
import javax.annotation.Nullable;

import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;


/**
 * http header 工具集
 *
 * @author xiaohuzi
 */
public final class HeaderUtil {

    private static final String HTTP_HEADER_ATTR = HeaderUtil.class.getName() + "HTTP_HEADER_ATTR";

    private static final String[] MOBILE_KEYWORDS = {
            "android", "iphone", "ipod", "ipad", "windows phone", "mqqbrowser", "iuc", "wget", "curl", "axel", "xiaomi",
            "redmi", "blackberry", "nexus", "htc", "huawei", "kindle", "lenovo", "lg", "motorola", "nokia",
            "oneplus", "playstation", "samsung", "xperia", "zte", "phone"};


    private HeaderUtil() {
    }


    public static HeaderEntity buildFromHttp(HttpServletRequest request, @Nullable HttpServletResponse response) {

        Long uid = 0L;

        Cookie[] cookies;
        cookies = request.getCookies();

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("uid".equals(cookie.getName())) {
                    Long parseLong = BaseUtil.tryParseLong(cookie.getValue());
                    if (null != parseLong) {
                        uid = parseLong;
                    }
                }

                //do other handle
            }
        }


        return HeaderEntity.builder()
                .uid(uid)
                .requestUrl(request.getRequestURL().toString())
                .requestMethod(request.getMethod())
                .remoteIp(getRemoteIp(request))
                .referer(Strings.nullToEmpty(request.getHeader(HttpHeaders.REFERER)))
                .userAgent(Strings.nullToEmpty(request.getHeader(HttpHeaders.USER_AGENT)))
                .build();

    }

    public static void setHttpHeaderAttr(HttpServletRequest request, HeaderEntity header) {
        request.setAttribute(HTTP_HEADER_ATTR, header);
    }


    public static HeaderEntity getHttpHeaderAttrFromRequest(HttpServletRequest request) {
        return (HeaderEntity) request.getAttribute(HTTP_HEADER_ATTR);
    }


    public static HeaderEntity getHttpHeaderAttrFromRequest(NativeWebRequest request) {
        return (HeaderEntity) request.getAttribute(HTTP_HEADER_ATTR, RequestAttributes.SCOPE_REQUEST);
    }


    public static boolean isFromMobile(HeaderEntity header) {
        String userAgent = header.getUserAgent();
        if (userAgent.trim().isEmpty()) {
            return false;
        }
        if (userAgent.contains("Windows NT")) {
            return false;
        }
        if (userAgent.contains("Macintosh")) {
            return false;
        }
        userAgent = userAgent.toLowerCase();
        for (String keyword : MOBILE_KEYWORDS) {
            if (userAgent.contains(keyword)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 请求是否来自微信
     *
     * @param header
     * @return
     */
    public static boolean isFromWeixin(HeaderEntity header) {
        return header.getUserAgent().contains("MicroMessenger");
    }

    /**
     * 获取请求IP
     *
     * @param request
     * @return
     */
    private static String getRemoteIp(HttpServletRequest request) {
        String ip;

        if ("nws".equals(request.getHeader("via"))) {//走深圳那边的反向代理，手动添加的via
            ip = request.getHeader("x-user-ip");//走深圳那边的反向代理，手动添加的x-user-ip
            if (!Strings.isNullOrEmpty(ip) && InetAddresses.isInetAddress(ip)) {
                return ip;
            }
        }

        ip = request.getHeader("X-Real-IP");

        if (!Strings.isNullOrEmpty(ip) && InetAddresses.isInetAddress(ip)) {
            return ip;
        }

        ip = request.getHeader("X-Forwarded-For");

        if (!Strings.isNullOrEmpty(ip) && InetAddresses.isInetAddress(ip)) {
            return ip;
        }

        ip = request.getHeader("Proxy-Client-IP");

        if (!Strings.isNullOrEmpty(ip) && InetAddresses.isInetAddress(ip)) {
            return ip;
        }

        ip = request.getHeader("WL-Proxy-Client-IP");

        if (!Strings.isNullOrEmpty(ip) && InetAddresses.isInetAddress(ip)) {
            return ip;
        }
        return request.getRemoteAddr();
    }

}
