package com.xiaohuzi.test.util;

import javax.annotation.Nullable;
import com.xiaohuzi.test.constant.RetCode;

public final class Result<T> {

    private final int ret;
    private final String msg;
    private final T data;

    private Result(int ret, @Nullable String msg, @Nullable T data) {
        this.ret = ret;
        this.msg = msg;
        this.data = data;
    }

    public int ret() {
        return ret;
    }

    public String msg() {
        return msg;
    }

    public T data() {
        return data;
    }

    public int getRet() {
        return ret;
    }

    public String getMsg() {
        return msg;
    }

    public T getData() {
        return data;
    }

    public static <Response> Result<Response> of(int ret, String msg, Response data) {
        return new Result<>(ret, msg, data);
    }

    public static <Response> Result<Response> of(int ret, String msg) {
        return new Result<>(ret, msg, null);
    }

    public static <Response> Result<Response> of(RetCode retCode) {
        return new Result<>(retCode.ret(), retCode.defaultMsg(), null);
    }

    public static <Response> Result<Response> of(RetCode retCode, @Nullable String msg) {
        if (msg == null) {
            msg = retCode.defaultMsg();
        }
        return new Result<>(retCode.ret(), msg, null);
    }

    public static <Response> Result<Response> of(RetCode retCode, @Nullable String msg, Response data) {
        if (msg == null) {
            msg = retCode.defaultMsg();
        }
        return new Result<>(retCode.ret(), msg, data);
    }

    public static <Response> Result<Response> nestedOf(RetCode retCode, Result<?> nestedResult) {
        String msg = (nestedResult.msg() == null ? retCode.defaultMsg() : nestedResult.msg())
                + "(" + nestedResult.ret() + ")";
        return new Result<>(retCode.ret(), msg, null);
    }

    public static <Response> Result<Response> nestedOf(RetCode retCode, Result<?> nestedResult, Response data) {
        String msg = (nestedResult.msg() == null ? retCode.defaultMsg() : nestedResult.msg())
                + "(" + nestedResult.ret() + ")";
        return new Result<>(retCode.ret(), msg, data);
    }
}

