package com.xiaohuzi.test.util;

import com.xiaohuzi.test.constant.RouteInterceptor;
import org.springframework.util.AntPathMatcher;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * ant uri 处理工具
 *
 * @author xiaohuzi
 */
public class AntPathUtil {
    private static AntPathMatcher antPathMatcher;
    private static Map<String, RouteInterceptor[]> antRouteInterceptorCache = new LinkedHashMap<String, RouteInterceptor[]>();

    private AntPathUtil() {
    }

    public static AntPathMatcher instance() {
        if (antPathMatcher == null) {
            antPathMatcher = new AntPathMatcher();
        }
        return antPathMatcher;
    }

    /**
     * ant uri 拦截器映射缓存
     *
     * @return
     */
    public static Map<String, RouteInterceptor[]> antRouteInterceptorCache() {
        return antRouteInterceptorCache;
    }

}
