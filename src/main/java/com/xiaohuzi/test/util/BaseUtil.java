package com.xiaohuzi.test.util;

import com.google.common.primitives.Longs;

import javax.annotation.Nullable;

/**
 * 基础操作工具集
 *
 * @author xiaohuzi
 */
public final class BaseUtil {

    private BaseUtil() {
    }


    /**
     * 将字符串转为long
     *
     * @param stringNum
     * @return null or Long
     */
    public static Long tryParseLong(@Nullable String stringNum) {
        return stringNum == null ? null : Longs.tryParse(stringNum);
    }

}
