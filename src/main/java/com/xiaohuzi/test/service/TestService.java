package com.xiaohuzi.test.service;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Map;

@Scope("prototype")
@Service("TestService1")
public class TestService implements DisposableBean {

    //    private ThreadLocal<Integer> requestNum = ThreadLocal.withInitial(()->0);
    private int requestNum;
    private String dec;
    private Map<String, String> map;

    public TestService() {
        System.out.println("construct 执行");
    }

//    public TestService(int requestNum) {
//        this.requestNum = requestNum;
//    }

//    public void setRequestNum(ThreadLocal<Integer> requestNum) {
//        this.requestNum = requestNum;
//    }

    public void setRequestNum(int requestNum) {
        this.requestNum = requestNum;
    }

    public void foo() {
//        System.out.println(requestNum.get() + " - " + Thread.currentThread());
        System.out.println(requestNum);
    }

    //MainConfig中@Bean 的destroyMethod
//    public void destroyMethod(){
//        System.out.println("调用Bean的函数(destroyMethod)");
//    }

    @Override
    public void destroy() throws Exception {
        System.out.println("调用Bean的函数(destroy)");
    }
}
