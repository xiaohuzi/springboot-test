package com.xiaohuzi.test.service;

import com.xiaohuzi.test.message.user.GetUserList;

public interface UserService {
     GetUserList.GetUserListResponse list(GetUserList.GetUserListRequest request);
}
