package com.xiaohuzi.test.service.impl;

import com.xiaohuzi.test.component.AuthComponent;
import com.xiaohuzi.test.message.Basic.BasicRequest;
import com.xiaohuzi.test.message.Basic.BasicResponse;
import com.xiaohuzi.test.message.user.GetUserList.GetUserListRequest;
import com.xiaohuzi.test.message.user.GetUserList.GetUserListResponse;
import com.xiaohuzi.test.message.user.UserEntity;
import com.xiaohuzi.test.model.User;
import com.xiaohuzi.test.repository.UserRepository;
import com.xiaohuzi.test.service.UserService;
import com.xiaohuzi.test.constant.BasicRetCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Date;

@Service
public class UserServiceImpl implements UserService {

    @Resource(name = "userRepository")
    private UserRepository userRepository;

    private int rowCount = 10;

    @Autowired
    private AuthComponent auth;


    /**
     * 获取用户列表
     *
     * @param request
     * @return
     */
    public GetUserListResponse list(GetUserListRequest request) {
        BasicRequest basicRequest = request.getHeader();
        long reqId;
        reqId = basicRequest.getRequestId();
        long time;
        time = basicRequest.getTime();
        int page;
        page = request.getPage();

        try {
            auth.user();
        } catch (Exception e) {
            e.printStackTrace();
        }



//        todo 参数校验

        List<User> list;
        list = userRepository.list(page - 1, rowCount);

        GetUserListResponse.Builder responseBuilder = GetUserListResponse.newBuilder();
        BasicResponse.Builder basicBuilder = BasicResponse.newBuilder();

        basicBuilder.setCode(BasicRetCode.SUCCESS.ret());
        basicBuilder.setTime(new Date().getTime());
        basicBuilder.setMsg(BasicRetCode.SUCCESS.defaultMsg());

        list.forEach(user->{
            UserEntity.User.Builder userBuilder = UserEntity.User.newBuilder();
            userBuilder.setId(user.getId());
            userBuilder.setName(user.getUserName());
//            userBuilder.setNickname(user.getNickName());
            userBuilder.setNickname("");
            userBuilder.setGender(user.getGender().toProto());
            responseBuilder.addUsers(userBuilder.build());
        });

        responseBuilder.setHeader(basicBuilder.build());

        return responseBuilder.build();
    }

}
