package com.xiaohuzi.test.controller;

import com.google.protobuf.InvalidProtocolBufferException;
import com.xiaohuzi.test.message.Sms.SmsRequest;
import com.xiaohuzi.test.message.Sms.SmsResponse;
import com.xiaohuzi.test.message.user.UserEntity.Gender;
import com.xiaohuzi.test.message.user.UserEntity.User;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

@RestController
public class ProtoController {

    /**
     * proto 实体
     *
     * @throws InvalidProtocolBufferException
     */
    public void proto() throws InvalidProtocolBufferException {
        User.Builder builder = User.newBuilder();
        builder.setId(1);
        builder.setName("xiaohuzi");
        builder.setGender(Gender.MAN);
        builder.setNickname("xiaohuzi");

        User user = builder.build();
        System.out.println("before :" + user.toString());

        System.out.println("===========User Byte==========");
        for (byte b : user.toByteArray()) {
            System.out.print(b);
        }
        System.out.println();
        System.out.println(user.toByteString());
        System.out.println("================================");

        //模拟接收Byte[]，反序列化成User类
        byte[] byteArray = user.toByteArray();
        User p2 = User.parseFrom(byteArray);
        System.out.println("after :" + p2.toString());

    }

    /**
     * php 调用代码
     * $this->smsRequest = new SmsRequest();
     * $this->smsResponse = new SmsResponse();
     * $apiUrl = "http://localhost:8080/proto/req";
     * $this->smsRequest->setOrderId("1001"); // api mesh
     * $this->smsRequest->setTplId(15);
     * $this->smsRequest->setTplVersion(3);
     * $this->smsRequest->setMobiles("15811125678");
     * $this->smsRequest->setParameter(json_encode(["java", "python", "php"]));
     * $this->smsRequest->setType(1);
     * <p>
     * $header = ["Content-Type:application/x-protobuf"];
     * $result = HttpCurl::multiCurlPost($apiUrl, $this->smsRequest->serializeToString(), 30, $header);
     * <p>
     * $this->smsResponse->parseFromString($result);
     * print_r($this->smsResponse);
     * <p>
     * <p>
     * $msg = [
     * 'code' => $this->smsResponse->getResultCode(),
     * 'uuid' => $this->smsResponse->getOrderId(),
     * 'inside_uid' => $this->smsResponse->getInnerOrderId(),
     * 'suc_uid' => $this->smsResponse->getSucUid(),
     * 'msg' => $this->smsResponse->getResultMsg()
     * ];
     *
     * @param request
     * @param response
     */
    public void req(HttpServletRequest request, HttpServletResponse response) {

        try (
             ByteArrayOutputStream out = new ByteArrayOutputStream();
                ServletOutputStream output = response.getOutputStream();
                InputStream input = request.getInputStream()) {

            SmsRequest req;

            // 从字节数据解析
//            byte by[] = new byte[1024];
//            int len = 0;
//            while ((len = input.read(by)) != -1) {
//                out.write(by, 0, len);
//            }
//
//            for (Byte bit : out.toByteArray()) {
//                System.out.print(bit);
//            }
//
//            SmsRequest req = SmsRequest.parseFrom(out.toByteArray());

            // 从输入流里解析
            req = SmsRequest.parseFrom(input);

            System.out.println(req.getParameter());
            System.out.println(req.toString());

            SmsResponse.Builder resp = SmsResponse.newBuilder();
            resp.setOrderId("10001");
            resp.setResultCode(200);
            resp.setSucUid("spring boot test");
            resp.setResultMsg("ok");
            // 写入输出流
//            resp.build().writeTo(response.getOutputStream());
            output.write(resp.build().toByteArray());

        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    /**
     * 请求方式同上
     * @param request
     * @return
     */
    public SmsResponse sms(@RequestBody SmsRequest request) {

        String orderId;
        orderId = request.getOrderId();

        SmsResponse.Builder resp = SmsResponse.newBuilder();
        resp.setOrderId("10001");
        resp.setResultCode(200);
        resp.setSucUid("spring boot test");
        resp.setResultMsg("ok");
        resp.setFailMobiles("123");
        resp.setInnerOrderId("order id");
        resp.setSucUid("456");
        resp.setPadding(1);

        return  resp.build();

    }




}
