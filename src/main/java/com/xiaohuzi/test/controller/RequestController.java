package com.xiaohuzi.test.controller;

import com.xiaohuzi.test.constant.BasicRetCode;
import com.xiaohuzi.test.constant.Gender;
import com.xiaohuzi.test.message.Basic;
import com.xiaohuzi.test.message.Form;
import com.xiaohuzi.test.message.user.GetUserList;
import com.xiaohuzi.test.message.user.UserEntity;
import com.xiaohuzi.test.util.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * 接受不同请求方式的请求参数
 */
@RestController
public class RequestController {

    /**
     * get 请求  http://localhost:8080/req/get?username=java&type=11&applyId=200
     * @param username
     * @param type
     * @param applyId
     * @return
     */
    public Result<Form> get(
            @RequestParam(value = "username", required = true) String username,
            @RequestParam(value = "type", required = true) int type,
            @RequestParam(value = "applyId", required = true) Long applyId){

            Form form = Form.builder().
                    username(username).
                    type(type).
                    applyId(applyId).build();
            return Result.of(0, "ok", form);
    }

    /**
     * 表单pos提交
     * http://localhost:8080/req/form?applyId=200
     * http://localhost:8080/req/form
     * @return
     */
    public Result<Form> form(
            @RequestParam(value = "type", required = true) int type,
            @RequestParam(value = "username", required = true) String username,
            @RequestParam(value = "applyId", required = true) Long applyId) {

            Form form = Form.builder().
                    username(username).
                    type(type).
                    applyId(applyId).build();

        return Result.of(0, "ok", form);
    }

    /**
     * post + json
     * @param form
     * @return
     */
    public Result<Form> json(@RequestBody Form form) {
        System.out.printf("username: %s, type: %d, applyId: %d", form.getUsername(),  form.getType(), form.getApplyId());
        return Result.of(0, "ok", form);
    }

    /**
     * proto
     * @param request
     * @return
     */
    public GetUserList.GetUserListResponse proto(@RequestBody GetUserList.GetUserListRequest request) {
        Basic.BasicRequest basic = request.getHeader();
        System.out.printf("requestId: %d,time: %d, page: %d", basic.getRequestId(), basic.getTime(), request.getPage());

        Basic.BasicResponse.Builder basicResponse =  Basic.BasicResponse.newBuilder();
        basicResponse.setCode(BasicRetCode.SUCCESS.ret());
        basicResponse.setTime(new Date().getTime());
        basicResponse.setMsg(BasicRetCode.SUCCESS.defaultMsg());

        UserEntity.User.Builder userBuilder = UserEntity.User.newBuilder();
        userBuilder.setId(12);
        userBuilder.setName("java");
        userBuilder.setNickname("language");
        userBuilder.setGender(Gender.MAN.toProto());

        GetUserList.GetUserListResponse.Builder response = GetUserList.GetUserListResponse.newBuilder();
        response.addUsers(userBuilder.build());

        response.setHeader(basicResponse.build());

        return response.build();
    }

}
