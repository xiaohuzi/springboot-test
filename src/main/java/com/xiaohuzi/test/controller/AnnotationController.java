package com.xiaohuzi.test.controller;

import com.xiaohuzi.test.component.annotaiton.SelfAnnotation;
import com.xiaohuzi.test.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


@RestController
public class AnnotationController {

//    @Autowired(required = false)
    @Resource
    private TestService service;


    @SelfAnnotation(param1 = "java", param2 = "python")
    public String annotation() throws NoSuchMethodException {

        for (int i = 0; i < 10; i++) {
            service.foo();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        SelfAnnotation ann = AnnotationController.class.getMethod("annotation").getAnnotation(SelfAnnotation.class);
        System.out.println("param1:" + ann.param1() + " param2:" + ann.param2());
        return "自定义Annotation SelfAnnotation: param1: " + ann.param1();
    }
}
