package com.xiaohuzi.test.controller;

import com.xiaohuzi.test.component.AuthComponent;
import com.xiaohuzi.test.component.entity.TestConfigEntity;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ConfigController {

    private TestConfigEntity config;

    private AuthComponent auth;


    public ConfigController(TestConfigEntity config, AuthComponent auth) {
        this.config = config;
        this.auth = auth;
    }


    /**
     * 自定义配置文件测试
     *
     * @return
     */
    public String foo() {
        return config.getUsage();
    }

}
