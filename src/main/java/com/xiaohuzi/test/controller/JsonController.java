package com.xiaohuzi.test.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.google.gson.LongSerializationPolicy;
import com.google.gson.reflect.TypeToken;
import com.xiaohuzi.test.model.User;
import com.xiaohuzi.test.util.Result;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

@RestController
public class JsonController {

    public Map<String, String> type() {
        Map<String, String> list = new HashMap<>();
        list.put("one", "php");
        list.put("two", "java");
        return list;
    }

    /**
     * TypeToken 生成复合的 type类型，如 Map<String, String>
     *
     * @return
     * @throws IOException
     */
    public Map<String, String> parseReader() throws IOException {
        OkHttpClient http = new OkHttpClient();
        Request.Builder builder = new Request.Builder();
        String url = "http://localhost:8080/json/type";
        Request request = builder.url(url).get().build();
        Response response = http.newCall(request).execute();

        Gson gson = (new GsonBuilder()).setLongSerializationPolicy(LongSerializationPolicy.STRING).create();
        Type type = TypeToken.getParameterized(Map.class, String.class, String.class).getType();

        return gson.fromJson(response.body().charStream(), type);

//        JsonParser parser = new JsonParser();
//        String ret = response.body().string();
//        JsonObject jsonObject = parser.parse(ret).getAsJsonObject();
//        String one = jsonObject.get("one").getAsString();
//        return one;
    }

    public User user() {
        User user = new User();
        user.setId(10086L);
        user.setUserName("Java");
        user.setNickName("coffee baby");

        return user;
    }

    public Result<User> json() {
        User user = new User();
        user.setId(10086L);
        user.setUserName("Java");
        user.setNickName("coffee baby");

        return Result.of(0, "ok", user);
    }

}
