package com.xiaohuzi.test.controller;

import com.xiaohuzi.test.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class HelloWorldController {

    private TestService service;
    private int requestNum;

    @Autowired(required = false)
    /**
     * As of Spring Framework 4.3, an @Autowired annotation on such a constructor is no longer necessary
     * if the target bean only defines one constructor to begin with.
     * However, if several constructors are available, at least one must be annotated to teach the container which one to use.
     * 如果有2个及多个constructor编译会报错， @Autowired没有提供指定入口
     */
    public HelloWorldController(TestService service) {
        this.service = service;
        System.out.println("controller init");
    }

    public String index() {
        requestNum++;
        System.out.println(requestNum);

        service.setRequestNum(5);
        service.foo();
//        service.setNum(ThreadLocal.withInitial(()->5));

        return "Hello Word!";
    }


}
