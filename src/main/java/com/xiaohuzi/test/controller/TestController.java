package com.xiaohuzi.test.controller;

import org.springframework.web.bind.annotation.RestController;
import okhttp3.*;

import java.net.UnknownHostException;

@RestController
public class TestController {

    public String internetAddress() {
        String addr = "123.126.55.41";

        try {
            return Dns.SYSTEM.lookup(addr).toString();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        return "null";
    }

}
