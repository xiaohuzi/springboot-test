package com.xiaohuzi.test.controller;

import com.xiaohuzi.test.model.User;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.*;

/**
 * future 长连接测试
 */
@RestController
public class KeepAliveController {
    private ExecutorService executor = Executors.newFixedThreadPool(10);

    private ConcurrentMap<String, CompletableFuture<User>> userMap = new ConcurrentHashMap();

    public CompletableFuture<User> product() {
//        KeepAliveController<User> completableFuture = KeepAliveController.supplyAsync(() -> {
//            try {
//                Thread.sleep(5000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }, executor).whenComplete((res, ex) -> System.out.println("async method completely, res = " + res + ", ex = " + ex + ", " + Thread.currentThread().getName()));
//
//        System.out.println("before return: " + Thread.currentThread().getName());
//        return completableFuture;
        System.out.println("product start " + Thread.currentThread().getName());
        CompletableFuture<User> future = new CompletableFuture<>();
        this.userMap.put("user", future);
        return future;
    }

    public String consumer() {
        CompletableFuture<User> future = this.userMap.remove("user");
        User user = new User();
        user.setId(10087l);
        user.setUserName("python");
        user.setNum(102);
        future.complete(user);
        return "done";
    }

}
