package com.xiaohuzi.test.controller;

import com.xiaohuzi.test.component.AuthComponent;
import com.xiaohuzi.test.message.user.GetUserList.GetUserListRequest;
import com.xiaohuzi.test.message.user.GetUserList.GetUserListResponse;
import com.xiaohuzi.test.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class UserController {

    private UserService userService;

    private AuthComponent auth;

    @Autowired //todo 加与不加的区别, 自动装载了 UserServiceImpl
    public UserController(AuthComponent auth, UserService userService) {
        this.auth = auth;
        this.userService = userService;
    }


    /**
     * 用户列表
     *
     * @param request
     * @return
     */
    public GetUserListResponse list(@RequestBody GetUserListRequest request) {
        return userService.list(request);
    }

}
