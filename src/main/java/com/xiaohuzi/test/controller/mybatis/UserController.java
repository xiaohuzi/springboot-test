package com.xiaohuzi.test.controller.mybatis;

/**
 * 注解方式操作数据库
 */

import java.util.List;

import com.xiaohuzi.test.constant.Gender;
import com.xiaohuzi.test.repository.UserMapper;
import com.xiaohuzi.test.model.User;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

@RestController("mybatisUser")
public class UserController {

    @Autowired
    private UserMapper userMapper;

    public void save(User user) {
        user.setUserName("Python");
        user.setPassWord("123");
        user.setGender(Gender.MAN);
        user.setNickName("script language");
        userMapper.add(user);
    }

    public void update(@PathVariable("id") Long id) {
        User user = new User();
        user.setId(id);
        user.setUserName("Go");
        user.setPassWord("456");
        user.setGender(Gender.MAN);
        user.setNickName("service language");
        userMapper.update(user);
    }

    public User get(@RequestParam("id") Long id) {
        return userMapper.findOne(id);
    }

    public List<User> list() {
        List<User> users;
        users = userMapper.getAll();
        return users;
    }

}
