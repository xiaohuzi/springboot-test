package com.xiaohuzi.test.controller.mybatis;

/**
 * xml方式操作数据库
 */

import com.xiaohuzi.test.constant.Gender;
import com.xiaohuzi.test.repository.UserRepository;
import com.xiaohuzi.test.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("mybatisXml")
public class XmlUserController {

    @Autowired
    private UserRepository userMapper;

    public void save(User user) {
        user.setUserName("Python");
        user.setPassWord("123");
        user.setGender(Gender.MAN);
        user.setNickName("script language");
        user.setNum(1003);
        userMapper.add(user);
    }

    public void update(@PathVariable("id") Long id) {
        User user = new User();
        user.setId(id);
        user.setUserName("Go");
        user.setNickName("service language");
        userMapper.update(user);
    }

    public User get(@RequestParam("id") Long id) {
        return userMapper.findOne(id);
    }

    public List<User> list() {
        List<User> users;
        users = userMapper.getAll();
        return users;
    }

}
