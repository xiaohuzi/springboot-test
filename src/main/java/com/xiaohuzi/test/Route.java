package com.xiaohuzi.test;

import com.xiaohuzi.test.component.entity.RouteEntity;
import com.xiaohuzi.test.component.route.RouteHelp;
import com.xiaohuzi.test.constant.RouteInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * http请求路由配置文件， 用来替代方法上的@RequestMapping
 * 进行认证、鉴权等其他 interceptor操作
 * <p>
 * url: http://localhost:8080/
 * url: http://localhost:8080/index
 * url: http://localhost:8080/anything/html
 * route.put("HelloWorldController@index", RouteEntity.builder()
 * .value(new String[]{"/", "index", "/anything/html"})
 * .method(new RequestMethod[]{RequestMethod.GET}).build()
 * );
 * <p>
 * url: http://localhost:8080/mybatis/xml-user/list
 * route.put("XmlUserController@list", RouteEntity.builder()
 * .value(new String[]{"xml-user/list"})
 * .method(new RequestMethod[]{RequestMethod.GET}).build()
 * ant path
 * url: http://localhost:8080/mybatis/user/api/foo/list
 * route.put("UserController@list", RouteEntity.builder()
 * .value(new String[]{"user/\*\*\/list"}).build()
 * );
 * <p>
 * <p>
 * 路由规则：
 * 1. namespaceMap和 uri prefixMap是一对一关系
 * 2. 一个命名空间对应一组route map
 * <p>
 * RequestMapping.value 参见 https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/web/bind/annotation/RequestMapping.html#value--
 *
 * @author xiaohuzi
 */
@Component
public class Route {

    private RouteHelp help;

    @Autowired
    public void setRouteHelp(RouteHelp help) {
        this.help = help;
    }

    /**
     * 请求路由配置
     * 默认请求方法 RequestMethod.POST
     *
     * @return
     */
    public void mapping() {

        help.namespace("com.xiaohuzi.test.controller").prefix("/").interceptor(new RouteInterceptor[]{RouteInterceptor.AUTH, RouteInterceptor.PERMISSION}).
                route(route -> {
                    route.put("HelloWorldController@index", RouteEntity.builder()
                            .value(new String[]{"/", "/index", "/anything/html"})
                            .method(new RequestMethod[]{RequestMethod.GET}).build()
                    );
                    route.put("AnnotationController@annotation", RouteEntity.builder()
                            .value(new String[]{"/annotation"})
                            .method(new RequestMethod[]{RequestMethod.GET}).build()
                    );
                    route.put("UserController@list", RouteEntity.builder()
                            .value(new String[]{"/user/list"})
                            .method(new RequestMethod[]{RequestMethod.GET}).build()
                    );
                    route.put("ProtoController@proto", RouteEntity.builder()
                            .value(new String[]{"/proto/data"})
                            .method(new RequestMethod[]{RequestMethod.GET}).build()
                    );
                    route.put("ProtoController@req", RouteEntity.builder()
                            .value(new String[]{"/proto/req"}).build()
                    );
                    route.put("ProtoController@sms", RouteEntity.builder()
                            .value(new String[]{"/proto/sms"}).build()
                    );
                    route.put("ConfigController@foo", RouteEntity.builder()
                            .value(new String[]{"/config/foo"})
                            .method(new RequestMethod[]{RequestMethod.GET}).build()
                    );
                    route.put("TestController@internetAddress", RouteEntity.builder()
                            .value(new String[]{"/test/inetaddr"})
                            .method(new RequestMethod[]{RequestMethod.GET}).build()
                    );
                    route.put("TestController@log", RouteEntity.builder()
                            .value(new String[]{"/test/log"})
                            .method(new RequestMethod[]{RequestMethod.GET}).build()
                    );
                    route.put("JsonController@type", RouteEntity.builder()
                            .value(new String[]{"/json/type"})
                            .method(new RequestMethod[]{RequestMethod.GET}).build()
                    );
                    route.put("JsonController@parseReader", RouteEntity.builder()
                            .value(new String[]{"/json/reader"})
                            .method(new RequestMethod[]{RequestMethod.GET}).build()
                    );
                    route.put("JsonController@user", RouteEntity.builder()
                            .value(new String[]{"/json/user"})
                            .method(new RequestMethod[]{RequestMethod.GET}).build()
                    );
                    route.put("JsonController@json", RouteEntity.builder()
                            .value(new String[]{"/json/json"})
                            .method(new RequestMethod[]{RequestMethod.GET}).build()
                    );
                    route.put("KeepAliveController@product", RouteEntity.builder()
                            .value(new String[]{"/test/product"})
                            .method(new RequestMethod[]{RequestMethod.GET}).build()
                    );
                    route.put("KeepAliveController@consumer", RouteEntity.builder()
                            .value(new String[]{"/test/consumer"})
                            .method(new RequestMethod[]{RequestMethod.GET}).build()
                    );
                    /* request params */
                    route.put("RequestController@get", RouteEntity.builder()
                            .value(new String[]{"/req/get"})
                            .method(new RequestMethod[]{RequestMethod.GET}).build()
                    );
                    route.put("RequestController@form", RouteEntity.builder()
                            .value(new String[]{"/req/form"}).build()
                    );
                    route.put("RequestController@json", RouteEntity.builder()
                            .value(new String[]{"/req/json"}).build()
                    );
                    route.put("RequestController@proto", RouteEntity.builder()
                            .value(new String[]{"/req/proto"}).build()
                    );
                });


        help.namespace("com.xiaohuzi.test.controller.mybatis").prefix("/mybatis").interceptor(new RouteInterceptor[]{RouteInterceptor.AUTH, RouteInterceptor.PERMISSION}).
                route(route -> {
                    route.put("XmlUserController@list", RouteEntity.builder()
                            .value(new String[]{"xml-user/list"})
                            .method(new RequestMethod[]{RequestMethod.GET}).build()
                    );
                    route.put("XmlUserController@save", RouteEntity.builder()
                            .value(new String[]{"xml-user/add"})
                            .method(new RequestMethod[]{RequestMethod.GET}).build()
                    );
                    route.put("XmlUserController@update", RouteEntity.builder()
                            .value(new String[]{"xml-user/update/{id}"})
                            .method(new RequestMethod[]{RequestMethod.GET}).build()
                    );
                    route.put("XmlUserController@get", RouteEntity.builder()
                            .value(new String[]{"xml-user/get"})
                            .method(new RequestMethod[]{RequestMethod.GET}).build()
                    );
                    route.put("UserController@list", RouteEntity.builder()
                            .value(new String[]{"user/**/list"})
                            .method(new RequestMethod[]{RequestMethod.GET}).build()
                    );
                    route.put("UserController@save", RouteEntity.builder()
                            .value(new String[]{"user/add"})
                            .method(new RequestMethod[]{RequestMethod.GET}).build()
                    );
                    route.put("UserController@update", RouteEntity.builder()
                            .value(new String[]{"user/update/{id}"})
                            .method(new RequestMethod[]{RequestMethod.GET}).build()
                    );
                    route.put("UserController@get", RouteEntity.builder()
                            .value(new String[]{"user/**/get"})
                            .method(new RequestMethod[]{RequestMethod.GET}).build()
                    );

                });
    }


    /**
     * 获取路由助手
     *
     * @return
     */
    public RouteHelp getRouteHelp() {
        return help;
    }

}
