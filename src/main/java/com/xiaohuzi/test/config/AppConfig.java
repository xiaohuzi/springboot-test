package com.xiaohuzi.test.config;

import com.xiaohuzi.test.component.entity.TestConfigEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;


/**
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *   不推荐 application.yml配置文件 添加自定义属性
 *   自定义配置文件使用 properties文件，存放路径src/main/resources/properties
 *   对应解析器存放在 com/xiaohuzi/test/config 路径
 *  参见 https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/context/annotation/PropertySource.html
 *
 * A note on property overriding with @PropertySource !!!
 * In cases where a given property key exists in more than one .properties file, the last @PropertySource annotation processed will 'win' and override.
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 */
@Configuration
@PropertySource(value = {
        "classpath:properties/test.properties",
        //add more
})
public class AppConfig {

    private Environment env;

    @Autowired
    public void environment(Environment env) {
        this.env = env;
    }


    /**
     * test 配置
     * @return
     */
    @Bean
    public TestConfigEntity testConfigEntity() {
        TestConfigEntity config = new TestConfigEntity();
        config.setUsage(env.getProperty("test.usage"));
        return config;
    }

    // add your config info
}

//@Component
//public class SelfProperty {
//
//    @Value("${com.xiaohuzi.title}")
//    private String title;
//
//    @Value("${com.xiaohuzi.description}")
//    private String description;
//
//
//    public String getTitle() {
//        return this.title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public String getDescription() {
//        return this.description;
//    }
//
//    public void setDescription(String description) {
//        this.description = description;
//    }
//}
