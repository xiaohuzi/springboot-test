# Getting Started

### 代码目录结构
参考 Web 金字塔式开发框架分层模型概述
https://learnku.com/articles/5813/overview-of-hierarchical-model-for-web-pyramid-development-framework 

#### 构建方式 maven
重写@RequestMapping加载方式

#### 路由配置文件
src/main/java/com/xiaohuzi/test/Route.java

#### 路由解析组件
src/main/java/com/xiaohuzi/test/component/RouteComponent.java

#### demo sql文件
src/main/resources/sql/javaweb.sql


待续...

